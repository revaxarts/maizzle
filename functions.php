<?php

ini_set( 'display_errors', false );

function trim_characters( $text, $length = 45, $append = '&hellip;' ) {

	$length = (int) $length;
	$text = trim( strip_tags( $text ) );

	if ( strlen( $text ) > $length ) {
		$text = substr( $text, 0, $length + 1 );
		$words = preg_split( '/[\s]|&nbsp;/', $text, -1, PREG_SPLIT_NO_EMPTY );
		preg_match( '/[\s]|&nbsp;/', $text, $lastchar, 0, $length );
		if ( empty( $lastchar ) ) {
			array_pop( $words );
		}

		$text = implode( ' ', $words ) . $append;
	}

	return $text;
}

function trim_words( $text, $limit, $link = true ) {

	static $count;

	$seed = 'abcdef';

	if ( is_null( $text ) ) {
		$text = words();
	}

	$wordcount = str_word_count( $text, 0 );
	$count++;
	$hash = md5( $seed . $count . $text . $limit );

	// get number
	$offset = ( round( abs( crc32( $hash ) ) % ($wordcount -$limit) ) + 1 );

	if ( $wordcount > $limit ) {
		$words = str_word_count( $text, 2 );
		$pos = array_keys( $words );
		$text = substr( $text, $pos[ $offset ], $pos[ $limit -1 ] );
		$text = ucwords( $words[ $pos[ $offset -1 ] ] ) . ' ' . trim( $text ) . '.';
		$orgtext = $text;
		if ( $link ) {
			$words = str_word_count( $text, 2 );
			$pos = array_keys( $words );
			$wordstolink = 3;
			$linkat = ( round( abs( crc32( $hash ) ) % ($limit - $wordstolink) ) + 1 );
			$text = substr( $text, 0, $pos[ $linkat ] ) . '[' . substr( $text, $pos[ $linkat ], strlen( $text ) - $pos[ $linkat ] );
			$text = substr( $text, 0, $pos[ $linkat + $wordstolink ] ) . ']' . substr( $text, $pos[ $linkat + $wordstolink ], strlen( $text ) - $pos[ $linkat + $wordstolink] );

			$text = str_replace(array('[', ']') , array('<a href="#">', '</a>'), $text);
			//$text = substr( $text, 0, $pos[ $linkat + $wordstolink +4 ] ) . '|YY|' . substr( $text, $pos[ $linkat + $wordstolink + 4 ], strlen( $text ) - $pos[ $linkat + $wordstolink] );
		}
	}

	return $text;
}

function sanitize_class_name( $name, $prefix = '' ) {

	$name = strtolower( $name );
	$name = preg_replace( '/[^a-z0-9_\-\/ ]/', '', $name );
	$name = str_replace( '/', '-', $name );
	$name = str_replace( ' ', '-', $name );

	return $prefix . $name;

}

function is_dev() {
	return getenv( 'NODE_ENV' ) == 'development';
}
function is_prod() {
	return getenv( 'NODE_ENV' ) == 'production';
}

function get_col_width( $c ) {

	$cols = page()->columns;

	$padding = page()->padding;
	$bodypadding = page()->bodypadding;
	$width = page()->width;

	$colwidth = ($width -(2 * $bodypadding) + $padding) / $cols;

	$columns = [];

	for ( $i = 0; $i < $cols ; $i++ ) {
		$columns[ $i ] = round( $colwidth * $i + ($colwidth -$padding) );
	}
	// $columns = [
	// 1 => 35,
	// 2 => 90,
	// 3 => 145,
	// 4 => 200,
	// 5 => 255,
	// 6 => 310,
	// 7 => 365,
	// 8 => 420,
	// 9 => 475,
	// 10 => 530,
	// 11 => 585,
	// 12 => 640,
	// ];
	// $columns = [
	// 1 => 35,
	// 2 => 90,
	// 3 => 145,
	// 4 => 200,
	// 5 => 255,
	// 6 => 320,
	// 7 => 365,
	// 8 => 420,
	// 9 => 475,
	// 10 => 530,
	// 11 => 585,
	// 12 => 640,
	// ];
	return isset( $columns[ $c -1 ] ) ? $columns[ $c -1 ] : $columns[ $cols ];
}

function page( $obj = null ) {
	global $my_page;

	if ( is_a( $obj, 'TightenCo\Jigsaw\PageVariable' ) ) {
		$my_page = $obj;
		return;
	}

	return $my_page;
}


function words() {

	$moby_dick = 'I sat down on the side of the bed, and commenced thinking about this head-peddling harpooneer, and his door mat. After thinking some time on the bed-side, I got up and took off my monkey jacket, and then stood in the middle of the room thinking. I then took off my coat, and thought a little more in my shirt sleeves. But beginning to feel very cold now, half undressed as I was, and remembering what the landlord said about the harpooneer’s not coming home at all that night, it being so very late, I made no more ado, but jumped out of my pantaloons and boots, and then blowing out the light tumbled into bed, and commended myself to the care of heaven. Whether that mattress was stuffed with corn-cobs or broken crockery, there is no telling, but I rolled about a good deal, and could not sleep for a long time. At last I slid off into a light doze, and had pretty nearly made a good offing towards the land of Nod, when I heard a heavy footfall in the passage, and saw a glimmer of light come into the room from under the door. Lord save me, thinks I, that must be the harpooneer, the infernal head-peddler. But I lay perfectly still, and resolved not to say a word till spoken to. Holding a light in one hand, and that identical New Zealand head in the other, the stranger entered the room, and without looking towards the bed, placed his candle a good way off from me on the floor in one corner, and then began working away at the knotted cords of the large bag I before spoke of as being in the room. I was all eagerness to see his face, but he kept it averted for some time while employed in unlacing the bag’s mouth. This accomplished, however, he turned round—when, good heavens! what a sight! Such a face! It was of a dark, purplish, yellow colour, here and there stuck over with large blackish looking squares. Yes, it’s just as I thought, he’s a terrible bedfellow; he’s been in a fight, got dreadfully cut, and here he is, just from the surgeon. But at that moment he chanced to turn his face so towards the light, that I plainly saw they could not be sticking-plasters at all, those black squares on his cheeks. They were stains of some sort or other. At first I knew not what to make of this; but soon an inkling of the truth occurred to me. I remembered a story of a white man—a whaleman too—who, falling among the cannibals, had been tattooed by them. I concluded that this harpooneer, in the course of his distant voyages, must have met with a similar adventure. And what is it, thought I, after all! It’s only his outside; a man can be honest in any sort of skin. But then, what to make of his unearthly complexion, that part of it, I mean, lying round about, and completely independent of the squares of tattooing. To be sure, it might be nothing but a good coat of tropical tanning; but I never heard of a hot sun’s tanning a white man into a purplish yellow one. However, I had never been in the South Seas; and perhaps the sun there produced these extraordinary effects upon the skin. Now, while all these ideas were passing through me like lightning, this harpooneer never noticed me at all. But, after some difficulty having opened his bag, he commenced fumbling in it, and presently pulled out a sort of tomahawk, and a seal-skin wallet with the hair on. Placing these on the old chest in the middle of the room, he then took the New Zealand head—a ghastly thing enough—and crammed it down into the bag. He now took off his hat—a new beaver hat—when I came nigh singing out with fresh surprise. There was no hair on his head—none to speak of at least—nothing but a small scalp-knot twisted up on his forehead. His bald purplish head now looked for all the world like a mildewed skull. Had not the stranger stood between me and the door, I would have bolted out of it quicker than ever I bolted a dinner. Even as it was, I thought something of slipping out of the window, but it was the second floor back. I am no coward, but what to make of this head-peddling purple rascal altogether passed my comprehension. Ignorance is the parent of fear, and being completely nonplussed and confounded about the stranger, I confess I was now as much afraid of him as if it was the devil himself who had thus broken into my room at the dead of night. In fact, I was so afraid of him that I was not game enough just then to address him, and demand a satisfactory answer concerning what seemed inexplicable in him. Meanwhile, he continued the business of undressing, and at last showed his chest and arms. As I live, these covered parts of him were checkered with the same squares as his face; his back, too, was all over the same dark squares; he seemed to have been in a Thirty Years’ War, and just escaped from it with a sticking-plaster shirt. Still more, his very legs were marked, as if a parcel of dark green frogs were running up the trunks of young palms. It was now quite plain that he must be some abominable savage or other shipped aboard of a whaleman in the South Seas, and so landed in this Christian country. I quaked to think of it. A peddler of heads too—perhaps the heads of his own brothers. He might take a fancy to mine—heavens! look at that tomahawk! But there was no time for shuddering, for now the savage went about something that completely fascinated my attention, and convinced me that he must indeed be a heathen. Going to his heavy grego, or wrapall, or dreadnaught, which he had previously hung on a chair, he fumbled in the pockets, and produced at length a curious little deformed image with a hunch on its back, and exactly the colour of a three days’ old Congo baby. Remembering the embalmed head, at first I almost thought that this black manikin was a real baby preserved in some similar manner. But seeing that it was not at all limber, and that it glistened a good deal like polished ebony, I concluded that it must be nothing but a wooden idol, which indeed it proved to be. For now the savage goes up to the empty fire-place, and removing the papered fire-board, sets up this little hunch-backed image, like a tenpin, between the andirons. The chimney jambs and all the bricks inside were very sooty, so that I thought this fire-place made a very appropriate little shrine or chapel for his Congo idol.';

	return $moby_dick;

	$lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla mollis tortor a scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pharetra massa sed orci pulvinar porta vehicula vitae elit. Suspendisse sed augue leo. Duis laoreet cursus sem in vulputate. Curabitur ullamcorper tincidunt mi nec malesuada. In convallis elit id ligula pulvinar tincidunt. In in nibh metus, ultricies viverra ante. In semper fringilla sem non interdum. Nulla at urna id urna bibendum vestibulum. Nunc aliquam turpis euismod est egestas dignissim condimentum mi vestibulum. Nam blandit dolor eget sapien tempor porttitor. Nam sollicitudin pharetra erat ac laoreet. Ut ac diam purus. Sed nec felis sed justo pretium faucibus sed non tellus. In sit amet lorem neque, in euismod velit. Donec turpis orci, accumsan ac varius eu, congue suscipit velit. Cras non risus sed velit auctor adipiscing at vel odio. Nullam tellus tellus, pretium id dignissim eget, tincidunt blandit ipsum. Sed tincidunt volutpat consequat. Vivamus tincidunt, justo vel eleifend pretium, est quam aliquam metus, ut cursus nulla neque ac massa. Mauris risus dui, viverra sollicitudin aliquet quis, elementum nec nulla. Aenean mattis sapien pretium nunc tincidunt vitae scelerisque augue laoreet. Curabitur egestas erat eu orci ullamcorper eu volutpat purus tincidunt. Nunc mattis tristique lectus, non pulvinar odio hendrerit et. Vivamus laoreet scelerisque massa, at adipiscing dui rutrum ut. In a mauris eu nibh elementum placerat. Suspendisse vel nunc justo, a pulvinar magna. Ut odio felis, fermentum et adipiscing eu, elementum at nisl. Mauris id tellus vel lectus venenatis euismod ac sed dolor. Phasellus varius rutrum varius. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam vel enim ante, facilisis bibendum leo. Proin quis lacus non tortor rhoncus condimentum. Mauris a dui id est rhoncus gravida sit amet lacinia magna. Proin quis pulvinar sem. Pellentesque convallis purus at purus tempor congue. Morbi sed justo at turpis sollicitudin ultrices. Vivamus bibendum velit eu turpis pellentesque fermentum.';

	return $lorem;

}
