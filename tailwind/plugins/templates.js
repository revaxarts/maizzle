module.exports = ({ addVariant }) => {
    addVariant('foobar', ({ modifySelectors }) => {
       modifySelectors(({ className }) => {
            return `.foobar .${className}`
        })
    })
}