---
extends: _layouts.mailster
singletag: h3
googleFonts: [Roboto,Merriweather,Quicksand]
---

{{ page($page) }}

@modules

	@module(['name' => 'Full Size Image'])

		@col(['class' => 'text-center'])
			@img
			@endimg
		@endcol

	@endmodule

	@module(['name' => 'Separator'])
		@col
			@separator @endseparator
		@endcol
	@endmodule

	@module(['name' => 'Full Size Text', 'bgimage' => true, 'class' => 'w-full'])

		@col(['class' => 'text-center'])
			@spacer (130)
			@single(['tag' => 'h1', 'class' => 'bg-green']) @endsingle
			@multi @endmulti
			@spacer (130)
		@endcol

	@endmodule

	@module(['name' => 'Full Size Text', 'class' => 'w-full'])

		@col(['class' => 'text-center'])
			@spacer (70)
			@single(['tag' => 'h1']) @endsingle
			@multi @endmulti
			@button(['align' => 'center']) @endbutton
			@spacer (70)
		@endcol

	@endmodule
	@module(['name' => 'Full Size Text', 'class' => 'sw-full'])

		@col(['class' => 'text-center'])
			@spacer (70)
			@single(['tag' => 'h1']) @endsingle
			@multi(['style' => 'font-size:15px;font-weight:bold']) @endmulti
			@spacer (70)
		@endcol

	@endmodule

	@module(['name' => 'Image on the Left', 'class' => ''])

		@col(['class' => 'w-1-2'])
			@img(['width' => 280])
			@endimg
		@endcol
		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@module(['name' => 'Image on the Right', 'reverse' => true])

		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-2'])
			@img(['width' => 280])
			@endimg
		@endcol

	@endmodule

	@module(['name' => 'BG Image on the Right', 'reverse' => true])

		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-2 sm-py-64', 'bgimage' => true])
			@wrap
				@single @endsingle
				@multi @endmulti
			@endwrap
		@endcol

	@endmodule

	@module(['name' => '1/3 Image on the Right', 'reverse' => true])

		@col(['class' => 'w-2-3'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-3 '])
			@img(['width' => 179]) @endimg
		@endcol

	@endmodule

	@module(['name' => '1/3 Image on the Left'])

		@col(['class' => 'w-1-3'])
			@img(['width' => 179]) @endimg
		@endcol
		@col(['class' => 'w-2-3'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@module(['name' => '1/1 Text'])

		@col([])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule
	@module(['name' => '1/2 Text'])

		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule
	@module(['name' => '1/3 Text'])

		@col(['class' => 'w-1-3'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule
	@module(['name' => '1/4 Text'])

		@col(['class' => 'w-1-4'])
			@wrap
				@single @endsingle
				@multi(['words' => 10]) @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-4'])
			@wrap
				@single @endsingle
				@multi(['words' => 10]) @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-4'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-4'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

@endmodules




@env('development')

@endenv

