---
headlinetag: h3
googleFonts: [Quicksand]
---

{{ page($page) }}


@modules

	@module(['name' => 'Full Size Text', 'class' => 'sw-full'])

		@col(['class' => 'text-center'])
			@spacer (70)
			@headline(['tag' => 'h1']) {headline} @endheadline
			@content(['style' => 'font-size:15px;font-weight:bold']) {content} @endcontent
			@spacer (70)
		@endcol

	@endmodule

@endmodules



@env('development')

@endenv

