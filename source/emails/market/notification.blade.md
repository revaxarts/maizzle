---
singletag: h3
googleFontss: [Quicksand]
---

{{ page($page) }}


@modules

	@module(['name' => 'Full Size Text', 'class' => 'sw-full'])

		@col(['class' => 'text-center'])
			@spacer (70)
			@single(['tag' => 'h1']) {headline} @endsingle
			@multi(['style' => 'font-size:15px;font-weight:bold']) {content} @endmulti
			@spacer (70)
		@endcol

	@endmodule

@endmodules



@env('development')

@endenv

