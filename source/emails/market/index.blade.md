---
extends: _layouts.market
slug: market
---

{{ page($page) }}

@modules

	@module(['name' => 'Full Size Image'])

		@col(['class' => 'text-center'])
			@img
			@endimg
		@endcol

	@endmodule

	@module(['name' => 'Separator'])
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => '1/2 Text'])

		@col
			@wrap
				@wrap(['class' => 'bg-black py-20 px-10'])
					@single(['tag' => 'h2', 'class' => 'text-white']) Headline @endsingle
				@endwrap
			@endwrap
		@endcol

	@endmodule

	@module(['name' => '1/2 Text'])

		@col(['class' => 'w-1-2'])
			@wrap
				@img(['width' => 280, 'class' => 'min-w-full'])
				@endimg
			@endwrap
		@endcol
		@col(['class' => 'w-1-2'])
			@wrap
				@img(['width' => 280, 'class' => 'min-w-full'])
				@endimg
			@endwrap
		@endcol

	@endmodule

	@module(['name' => '1/2 Text'])

		@col(['class' => 'w-1-2'])
			@wrap
				@img(['width' => 280, 'class' => 'min-w-full'])
				@endimg
				@wrap(['class' => 'bg-black py-20 px-10'])
					@single(['tag' => 'h2', 'class' => 'text-white']) @endsingle
				@endwrap
			@endwrap
		@endcol
		@col(['class' => 'w-1-2'])
			@wrap
				@img(['width' => 280, 'class' => 'min-w-full'])
				@endimg
				@wrap(['class' => 'bg-black py-20 px-10'])
					@single(['tag' => 'h2', 'class' => 'text-white']) @endsingle
				@endwrap
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => '1/2 Text'])

		@col(['class' => 'w-1-3'])
			@wrap
				@img(['width' => 179, 'class' => 'min-w-full'])
				@endimg
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@img(['width' => 179, 'class' => 'min-w-full'])
				@endimg
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@img(['width' => 179, 'class' => 'min-w-full'])
				@endimg
			@endwrap
		@endcol

	@endmodule

	@module(['name' => '1/2 Text'])

		@col(['class' => 'w-1-3'])
			@wrap
				@img(['width' => 179, 'class' => 'min-w-full'])
				@endimg
				@wrap(['class' => 'bg-black py-20 px-10'])
					@single(['tag' => 'h2', 'class' => 'text-white']) Headline @endsingle
				@endwrap
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@img(['width' => 179, 'class' => 'min-w-full'])
				@endimg
				@wrap(['class' => 'bg-black py-20 px-10'])
					@single(['tag' => 'h2', 'class' => 'text-white']) Headline @endsingle
				@endwrap
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@img(['width' => 179, 'class' => 'min-w-full'])
				@endimg
				@wrap(['class' => 'bg-black py-20 px-10'])
					@single(['tag' => 'h2', 'class' => 'text-white']) Headline @endsingle
				@endwrap
			@endwrap
		@endcol

	@endmodule

	@module(['name' => '1/2 Text'])

		@col(['class' => 'w-1-2'])
			@float(['class' => 'pr-10'])
				@img(['width' => 135])
				@endimg
			@endfloat
			@float
				@img(['width' => 135])
				@endimg
			@endfloat
		@endcol
		@col(['class' => 'w-1-2'])
			@float(['class' => 'pr-10'])
				@img(['width' => 135])
				@endimg
			@endfloat
			@float
				@img(['width' => 135])
				@endimg
			@endfloat
		@endcol

	@endmodule

	@module(['name' => 'Intro'])

		@col()
			@wrap(['class' => 'leading-32'])
				@single(['tag' => 'h1']) @endsingle
				@multi(['tag' => 'p', 'class' => 'text-lg']) @endmulti
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => 'Floating Image on the Left', 'class' => ''])

		@col
			@wrap(['align' => 'left'])
				@float(['align' => 'left', 'padding' => 20])
					@wrap(['class' => 'p-0 pb-20 pr-20 sm-pr-0 leading-full'])
						@img(['width' => 280, 'class' => 'min-w-full'])
						@endimg
					@endwrap
				@endfloat
				@single @endsingle
				@multi(['words' => 55]) @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => 'Floating Image on the Right', 'class' => ''])

		@col
			@wrap(['align' => 'left'])
				@float(['align' => 'right', 'padding' => 20])
					@wrap(['class' => 'p-0 pb-20 pl-20 sm-pl-0 leading-full'])
						@img(['width' => 280, 'class' => 'min-w-full'])
						@endimg
					@endwrap
				@endfloat
				@single @endsingle
				@multi(['words' => 55]) @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => 'Image on the Left', 'class' => ''])

		@col(['class' => 'w-1-2'])
			@img(['width' => 280, 'class' => 'min-w-full'])
			@endimg
		@endcol
		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => 'Image on the Right', 'reverse' => true])

		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-2'])
			@img(['width' => 280, 'class' => 'min-w-full'])
			@endimg
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => '1/3 Image on the Left'])

		@col(['class' => 'w-1-3'])
			@img(['width' => 179, 'class' => 'min-w-full']) @endimg
		@endcol
		@col(['class' => 'w-2-3'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => '1/3 Image on the Right', 'reverse' => true])

		@col(['class' => 'w-2-3'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@img(['width' => 179, 'class' => 'min-w-full']) @endimg
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => '1/1 Text'])

		@col([])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => '1/2 Text'])

		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-2'])
			@wrap
				@single @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@module(['name' => '1/3 Text'])

		@col(['class' => 'w-1-3'])
			@wrap
				@single Headline @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@single Headline @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-3'])
			@wrap
				@single Headline @endsingle
				@multi @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule

	@row
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule


	@module(['name' => '1/4 Text'])

		@col(['class' => 'w-1-4'])
			@wrap
				@single Headline @endsingle
				@multi(['words' => 15]) @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-4'])
			@wrap
				@single Headline @endsingle
				@multi(['words' => 15]) @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-4'])
			@wrap
				@single Headline @endsingle
				@multi(['words' => 15]) @endmulti
				@button @endbutton
			@endwrap
		@endcol
		@col(['class' => 'w-1-4'])
			@wrap
				@single Headline @endsingle
				@multi(['words' => 15]) @endmulti
				@button @endbutton
			@endwrap
		@endcol

	@endmodule



@endmodules


@env('development')

@endenv

