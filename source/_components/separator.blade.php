<?php
if(empty(trim($slot))){
	$slot = 'Read More';
}
?>
<!-- #separator -->
	<table class="separator w-full" cellpadding="0" cellspacing="0" role="presentation">
	  <tr>
	    <td>
	      <div>&zwnj;</div>
	    </td>
	  </tr>
	</table>
<!-- #/separator -->
