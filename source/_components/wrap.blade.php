<!-- #wrap -->
<table cellpadding="0" cellspacing="0" role="presentation" width="100%" align="{{ $align }}" valign="{{ $valign }}">
	<tr>
		<td class="wrap {{ $class }}">{{ $slot }}</td>
	</tr>
</table>
<!-- #/wrap -->
