<?php
$auto = ! isset( $auto ) || $auto == true ? 'auto' : '';
$active = isset( $active ) && $active == true ? 'active' : '';
	?>

<!-- #module -->
<module label="{{ $name }}" {{ $auto }} {{ $active }}>

	@row(['class' => $class, 'align' => $align, 'style' => $style, 'reverse' => $reverse, 'bgimage' => $bgimage, 'moduleclass' => sanitize_class_name($name, 'module-') . ' ' . $moduleclass])
		{{ $slot }}
	@endrow

</module>
<!-- #/module -->
