<?php
if ( ! isset( $width ) ) {
	$width = 600;
}
if ( ! isset( $height ) ) {
	$height = round($width/1.6);
}
if ( ! isset( $src ) ) {
	$src = 'https://dummyx.newsletter-plugin.com/'.($width).'x'.($height).'.jpg';
}

if(empty(trim($slot))){
	$slot = 'Read More';
}
?>
<!-- #button -->
<table class="textbutton {{ $class }}" cellpadding="0" cellspacing="0" role="presentation" align="{{ $align }}">
  <tr>
    <td class="">
      <a href="{{ $url ?? '#'}}" class="{{ $linkclass }}" editable>{{ $slot }}</a>
    </td>
  </tr>
</table>
<!-- #/button -->
