<?php
	if(!isset($align)){
		$align = 'left';
	}
 ?>
<!-- #float -->
<table class="sm-w-full" cellpadding="0" cellspacing="0" role="presentation" align="{{ $align }}">
	<tr>
		<td class="{{ $class }}">{{ $slot }}</td>
	</tr>
</table>
<!-- #/float -->
