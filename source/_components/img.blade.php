<?php
if ( ! isset( $width ) ) {
	$width = 600;
}
if ( ! isset( $height ) ) {
	$height = round( $width / 1.6 );
}
$editable = ! isset( $editable ) ? 'editable ' : '';

$label = $label ?? 'Image max width ' . $width . ' pixel';

if ( ! isset( $src ) ) {
	$src = 'https://dummyx.newsletter-plugin.com/' . ($width) . 'x' . ($height) . '.jpg';
} elseif ( is_file( $src ) ) {
	$size = getimagesize( $src );
}
?>
<!-- #img -->
<img src="{{ $src }}" class="{{ $class }}" alt="{{ $alt }}" width="{{ $width }}" border="0" {{ $editable }}label="{{ $label }}" />
<!-- #/img -->
