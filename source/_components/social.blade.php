<?php
if ( ! isset( $width ) ) {
	$width = 32;
}
if ( ! isset( $height ) ) {
	$height = $width;
}

if ( empty( trim( $slot ) ) ) {
	$slot = 'twitter';
}

$service = $slot;
$href = 'https://' . $service . '.com';
$src = 'img/social/dark/' . $service.'.png';

?>
<!-- #social -->
<a editable label="{{ $label ?? 'Social Media Button'}}" href="{{ $href }}" class="{{ $class }} no-underline">
	<img src="{{ $src }}" alt="" width="{{ $width }}" height="{{ $height }}" border="0">
</a>
<!-- #/social -->
