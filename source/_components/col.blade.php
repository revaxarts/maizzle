<!-- #col -->
 @if(!$bgimage)
  <td class="col {{ $class }}" align="{{ $align }}" style="{{ $style }}" valign="{{ $valign ?? 'top' }}">
	    {{ $slot }}
  </td>
@else
<?php
if ( 'string' == gettype( $bgimage ) ) {
	$url = $bgimage;
	$url2x = $bgimage;
}
if ( ! isset( $url ) ) {
	$url = 'https://dummyx.newsletter-plugin.com/' . ($img_width ?? 600) . 'x' . ($height ?? 340) . '.jpg';
}
?>
	<td class="col bgimage {{ $class }}" align="{{ $align }}" valign="{{ $valign ?? 'top' }}" background="{{ $url }}" style="background-image: url('{{ $url }}');{{ $style }}">
		  <!--[if gte mso 9]>
		  <v:image src="{{ $url }}" xmlns:v="urn:schemas-microsoft-com:vml" style="width:700px;height:500px;" />
		  <v:rect fill="false" stroke="false" style="position:absolute;width:700px;height:500px;">
		  <div>
		  <![endif]-->
	  <table cellpadding="0" cellspacing="0" role="presentation">
		  <tr>
		  	<td>
        	{{ $slot }}
        	</td>
        </tr>
      </table>
		 <!--[if gte mso 9]></div></v:rect><![endif]-->
	 </td>
@endif
<!-- #/col -->
