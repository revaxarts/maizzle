<!-- #row -->
<table class="module {{ $moduleclass }}" cellpadding="0" cellspacing="0" role="presentation">
  <tr>
    <td class="outerrow" align="center">
      <table class="row {{ $class }}" cellpadding="0" cellspacing="0" role="presentation" align="{{ $align }}" style="{{ $style }}">
@if($reverse)
       <tr class="sm-flex sm-flex-col-reverse">
@else
       <tr>
@endif
@if(!$bgimage)
	    {{ $slot }}
@else
<?php
if ( 'string' == gettype( $bgimage ) ) {
	$url = $bgimage;
	$url2x = $bgimage;
}
if ( ! isset( $url ) ) {
	$url = 'https://dummyx.newsletter-plugin.com/' . ($img_width ?? 600) . 'x' . ($height ?? 340) . '.jpg';
}
?>
	<td class="bgimage" background="{{ $url }}" style="background-image: url('{{ $url }}');">
		  <!--[if gte mso 9]>
		  <v:image src="{{ $url }}" xmlns:v="urn:schemas-microsoft-com:vml" style="width:700px;height:500px;" />
		  <v:rect fill="false" stroke="false" style="position:absolute;width:700px;height:500px;">
		  <div>
		  <![endif]-->
	  <table class="w-full" cellpadding="0" cellspacing="0" role="presentation">
		  <tr>
        	{{ $slot }}
        </tr>
      </table>
		 <!--[if gte mso 9]></div></v:rect><![endif]-->
	 </td>
@endif
        </tr>
      </table>
    </td>
  </tr>
</table>


<!-- #/row -->
