<?php
if ( empty( trim( $slot ) ) ) {
	$slot = trim_words(null, $words ?? 20);
}
	$tag = $tag ?? page()->contenttag ?? 'p';
	$label = $label ?? 'Body';
	$contenteditable = is_dev() ? ' contenteditable="true"' : ''
?>
<!-- #content -->
<multi{!! $contenteditable !!} label="{!! $label !!}"><{{ $tag }} class="{{ $class }}" style="{{ $style }}">{!! $slot !!}</{{ $tag }}></multi>
<!-- #/content -->
