<?php
if ( empty( trim( $slot ) ) ) {
	$slot = 'Headline goes here';
}
	$tag = $tag ?? page()->headlinetag ?? 'h2';
	$label = $label ?? 'Headline';
	$contenteditable = is_dev() ? ' contenteditable="true"' : ''
?>
<!-- #headline -->
@if($tag) <{{ $tag }} class="{!! $class !!}" style="{!! $style !!}">@endif
<single{!! $contenteditable !!} label="{!! $label !!}">{{ $slot }}</single>
@if($tag) </{{ $tag }}> @endif
<!-- #/headline -->
