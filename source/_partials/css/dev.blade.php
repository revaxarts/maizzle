<style>
modules{
	border-bottom:1px solid red;
	display: block;
}
module{
	border-top:1px solid red;
	display: block;
}
module:before{
	content: attr(label);
    display: block;
    font-size: 10px;
    position: absolute;
    background: #fff;
    padding: 2px;
    opacity: 0.4;
}
body, html, .wrapper{
	@apply font-sans bg-mailster-body ss;
}

.row{
	outline: 1px dotted green;
}
.row:hover{
	outline: 2px dotted green;
}
.col{
	outline: 1px dotted blue;
}
.col:hover{
	outline: 2px dotted blue;
}
.wrap{
	outline: 1px dotted red;
}
.row:hover:before, .col:hover:before{
	content: attr(class);
    display: block;
    font-size: 10px;
    position: absolute;
    background: #000;
    padding: 2px;
    opacity: 0.4;
}
.row:hover:before{
	content: '.row';
}
.col:hover:before{
}

</style>