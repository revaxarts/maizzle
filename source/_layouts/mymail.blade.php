@extends('_layouts.master')

@push('templateinfo')
<!--[mailster]

	Template Name: MyMail
	Template URI: https://mailster.co
	Description: This is the default newsletter template which comes with Mailster. For more premium templates <a href="https://rxa.li/mailstertemplates">click here</a>.
	Author: EverPress
	Author URI: https://everpress.io
	Version: 6.1

-->
@endpush
@push('head')
<style data-embed>
	 a,  a:visited, a:link {
		color:#5ca8cd;
		text-decoration: none;
	}
</style>
@endpush
@push('style')
@endpush

{{-- This comment will not be in the rendered HTML --}}

@push('header')

	@row(['moduleclass' => 'header', 'class' => 'bg-transparent'])

		@col(['class' => 'text-center'])
			@single(['tag' => 'div', 'class' => 'text-xs']) {webversion} | {unsub} | {profile} @endsingle
		@endcol

	@endrow

	@row(['moduleclass' => 'header'])

		@col(['class' => 'w-1-2 p-20 sm-text-center', 'valign' => 'bottom' ])
			@img(['width' => 100, 'src' => 'img/logo.png', 'label' => 'Logo'])
			@endimg
		@endcol
		@col(['class' => 'w-1-2 p-20 text-right sm-text-center', 'valign' => 'bottom'])
			@single(['tag' => false]) {subject} @endsingle
		@endcol

	@endrow

@endpush

@push('footer')

	@row(['class' => 'footer'])
		@col
			@separator @endseparator
		@endcol
	@endmodule

	@row(['class' => 'footer', 'reverse' => true])

		@col(['class' => 'w-2-3 sm-text-center'])
			@wrap(['class' => 'leading-16 text-xs'])
				@multi
					{can-spam}<br>
					{address}<br>
					{copyright}<br>
				@endmulti
			@endwrap
		@endcol
		@col(['class' => 'w-1-3 text-right sm-text-center'])
			@wrap()
				@buttons
					@social twitter @endsocial
					@social facebook @endsocial
				@endbuttons
			@endwrap
		@endcol

	@endrow

	@row(['moduleclass' => 'header', 'class' => 'bg-transparent'])

			@img(['width' => 600, 'src' => 'img/shadow.png', 'editable' => false])
			@endimg

	@endrow


@endpush