@extends('_layouts.master')

@push('head')
@endpush
@push('style')
@endpush

{{-- This comment will not be in the rendered HTML --}}

@push('header')

	@row(['moduleclass' => 'header', 'class' => 'bg-transparent'])

		@col(['class' => 'w-1-2 text-center'])
			@spacer (10)
			@img(['width' => 150, 'src' => 'https://mailster.co/wp-content/themes/mailster/img/logotype_blue.svg'])
			@endimg
			@spacer (50)
		@endcol

	@endrow

@endpush

@push('footer')

	@row(['class' => 'footer'])
		@col
			@separator @endseparator
		@endcol
	@endmodule

	@row(['class' => 'footer'])

		@col(['class' => 'w-1-3 text-xs'])
			@wrap
				@multi
					@tag(can-spam)
					@tag(address)
					@tag(copyright)
				@endmulti
			@endwrap
		@endcol
		@col(['class' => 'w-2-3 text-right'])
			@wrap(['class' => 'text-right'])
				@buttons
					@social twitter @endsocial
					@social twitter @endsocial
					@social twitter @endsocial
				@endbuttons
			@endwrap
		@endcol

	@endrow


@endpush