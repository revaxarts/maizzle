@extends('_layouts.master')

@push('templateinfo')
<!--[mailster]

	Template Name: Market
	Template URI: https://mailster.co
	Description: This is the default newsletter template which comes with Mailster. For more premium templates <a href="https://rxa.li/mailstertemplates">click here</a>.
	Author: EverPress
	Author URI: https://everpress.io
	Version: 6.1

-->
@endpush
@push('head')
<style data-embed>
	 a,  a:visited, a:link {
		color:#00A9E0;
		text-decoration: none;
	}
</style>
@endpush
@push('style')
@endpush

@push('header')

	@row(['moduleclass' => 'header', 'class' => 'bg-transparent'])

		@col(['class' => 'uppercase'])
			@single(['tag' => 'div', 'class' => 'text-xxs leading-12']) This line will show up in the preview email clients @endsingle
			@single(['tag' => 'div', 'class' => 'text-xxs leading-12']) Trouble seeing something? {webversion}, {unsub} or {profile} @endsingle
		@endcol

	@endrow

	@row(['moduleclass' => 'header'])

		@col(['class' => 'sm-text-center', 'valign' => 'bottom' ])
			@spacer(30)
			@img(['width' => 100, 'src' => 'img/logo.png', 'label' => 'Logo', 'class' => 'm-0'])
			@endimg
			@single(['tag' => 'h2', 'class' => 'm-0']) Newsletter for Mailster @endsingle
			@spacer(20)
		@endcol

	@endrow

@endpush

@push('footer')

	@row(['class' => 'footer'])
		@col
			@separator @endseparator
			@wrap(['class' => 'text-xxs text-right uppercase leading-12', 'valign' => 'top'])
				@single(['tag' => false]) // @endsingle
			@endwrap
		@endcol
	@endmodule

	@row(['class' => 'footer'])

		@col(['class' => 'w-3-4'])
			@wrap(['class' => 'text-xxs uppercase leading-12', 'valign' => 'top'])
				@multi
					{can-spam}<br>
					{address}<br>
					{copyright}<br>
				@endmulti
			@endwrap
		@endcol
		@col(['class' => 'w-1-4 text-right'])
			@wrap()
				@buttons
					@social twitter @endsocial
					@social facebook @endsocial
				@endbuttons
			@endwrap
		@endcol

	@endrow


@endpush